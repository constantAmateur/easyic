#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define PI 3.141592654
#define MAX_LEN_CHAR 2048
//Because we have to write in FORTRAN binary format, we have to put the size of whatever we're writing around the write statement...
#define BLKLEN my_fwrite(&blklen, sizeof(blklen), 1, output);
#define PRINT_VNUM printf("This is EasyIC, v1.0.6\n");


/*
The purpose of this program is to take plain text files as input and produce binary files for use with Gadget as output.  Each "block" (described below and in the Gadget documentation) gets its own file.  Each entry in the block is given its own line and if that entry is an array, each entry in the array is seperated by a tab character.  The program must be run with one paramater, which gives the location of a file containing the file names of all the blocks.

The Initial Conditions file contains a number of blocks (some of which are optional), that contain information about the setup and the particles.  Look at the GADGET documentation for more info.  They are:
Name								Used?														Description
Header							Always													Contains a bunch of metadat about the setup, look at struct io_header
Particle positions			Always													Positions of the particles, ordered by "type"
Particle velocities			Always													Velocities of the particles, ordered by "type"
Particle IDs					Always													Unique identifier for each particle
Variable particle masses	When there are particles with variable mass	The routine knows that this should be present when there are particles present that have mass 0.0 in the Massarr header entry
Internal energy				Ngas>0													Only used when there are gas particles in the simulation, as specified in Npart in the header.
Density							Ngas>0?													Comoving density of the SPH particles.  Used only for cosmology simulations?
Smoothing length				Ngas>0?													SPH smoothing length, h.  Probably used only when there are gass particles
Gravitational potential		If explicitly enabled in makefile				Gravitational potential for particles
Accelerations					If explicitly enabled in makefile				Accelerations of particles
Rate of entropy production	If explicitly enabled in makefile & Ngas>0?	Rate of change of the entropy function of each gas particle
Timesteps of particles		If explicitly enabled in makefile				Individual timesteps of particles.
*/




size_t my_fwrite(void *ptr, size_t size, size_t nmemb, FILE *stream);
struct io_header process_header(FILE *input,FILE *output);
void process_float_array(FILE *input, FILE *output,int blklen,int Ndim);
void process_uint_array(FILE *input,FILE *output,int blklen,int Ndim);

/*This is the header as described in the Gadget2 documentation.  It should be 256 bytes in total length.  Check that the definitions match the latest version*/
struct io_header
{
  unsigned int npart[6];                        /*!< number of particles of each type in this file */
  double mass[6];                      /*!< mass of particles of each type. If 0, then the masses are explicitly
                                        stored in the mass-block of the snapshot file, otherwise they are omitted */
  double time;                         /*!< time of snapshot file */
  double redshift;                     /*!< redshift of snapshot file */
  int flag_sfr;                        /*!< flags whether the simulation was including star formation */
  int flag_feedback;                   /*!< flags whether feedback was included (obsolete) */
  unsigned int npartTotal[6];          /*!< total number of particles of each type in this snapshot. This can be
                                        different from npart if one is dealing with a multi-file snapshot. */
  int flag_cooling;                    /*!< flags whether cooling was included  */
  int num_files;                       /*!< number of files in multi-file snapshot */
  double BoxSize;                      /*!< box-size of simulation in case periodic boundaries were used */
  double Omega0;                       /*!< matter density in units of critical density */
  double OmegaLambda;                  /*!< cosmological constant parameter */
  double HubbleParam;                  /*!< Hubble parameter in units of 100 km/sec/Mpc */
  int flag_stellarage;                 /*!< flags whether the file contains formation times of star particles */
  int flag_metals;                     /*!< flags whether the file contains metallicity values for gas and star particles */
  unsigned int nparttotalHighWord[6];  /*!< High word of the total number of particles of each type */
  int  flag_entropy_instead_u;         /*!< flags that IC-file contains entropy instead of u */
  char	fill[60];	               /*!< fills to 256 Bytes */
};

int main( int argc, char *argv[] )
{
	FILE *filenames, *header, *pos, *vel, *id, *mass, *energy, *density, *smoothing, *pot, *accel, *entropy, *time, *out;
	char record[MAX_LEN_CHAR], *fld1, *fld2; /*Array to hold each line*/
	struct io_header header_block; //We need this to calculate the block lengths for things other than the header
	int blklen,i;

	if (argc !=3 ) /* We should have two arguments, the program and the file */
	{
		if(argc==2 && (!strcmp(argv[1],"--version") || !strcmp(argv[1],"-v"))){
			PRINT_VNUM;
			return(1);
		}
		if(argc==2 && (!strcmp(argv[1],"--help") || !strcmp(argv[1],"-h"))){
			printf("\t%s takes a txt file with the names of all Gadget-2.0 \"blocks\" as input and outputs a Gadget-2.0 formatted binar. See the README and EXAMPLE for more information.\n",argv[0]);
			printf("\tThe usage is %s InputFile OutputFile\n",argv[0]);
			printf("\tPossible options are:\n");
			printf("\tInputFile \tA text file containing the locations of other ascii files with the Gadget-2.0 block information.\n");
			printf("\tOutputFile \tA file which will be created (or overwritten) that contains the Gadget-2.0 formatted binary\n");
			printf("\t--version (-v)\tPrints version number.\n");
			printf("\t--help (-h)\tThis help dialog.\n");
			return(1);
		}
		printf("usage: %s InputFilename OutputFilename\n\t%s --help for more info.\n",argv[0],argv[0]);
		return(1);
	}
	filenames=fopen(argv[1],"r");
	out=fopen(argv[2],"w");
	if (filenames ==0 || out==0 )
	{
		printf( "Could not open file\n");
		return(1);
	}
	/*Read tab seperated file containing the files we need...*/
	while(fgets(record, sizeof(record),filenames))
	{
		fld1=strtok(record,"\t");
		fld2=strtok(NULL,"\n");
		/*Check if the line is commented out*/
		if(*fld1 != '#'){
			/*Assign the different files to file handlers...*/
			if(strcmp(fld1,"Header")==0){
				printf("Opening file %s to retrieve header information...\n",fld2);
				header=fopen(fld2,"r");
				if(header==0){
					printf("Failed to open file!\n");
					return(1);
				}
				header_block=process_header(header,out);
				fclose(header);
			}else if(strcmp(fld1,"Pos")==0){
				printf("Opening file %s to retrieve position information...\n",fld2);
				pos=fopen(fld2,"r");
				if(pos==0){
					printf("Failed to open file!\n");
					return(1);
				}
				//Calculate the block size...
				blklen=0;
				for(i=0;i<6;i++) blklen+=header_block.npart[i];
				blklen *= 3*sizeof(float);
				process_float_array(pos,out,blklen,3);
				fclose(pos);
			}else if(strcmp(fld1,"Vel")==0){
				printf("Opening file %s to retrieve velocity information...\n",fld2);
				vel=fopen(fld2,"r");
				if(vel==0){
					printf("Failed to open file!\n");
					return(1);
				}
				//Calculate the block size...
				blklen=0;
				for(i=0;i<6;i++) blklen += header_block.npart[i];
				blklen *= 3*sizeof(float);
				process_float_array(vel,out,blklen,3);
				fclose(vel);
			}else if(strcmp(fld1,"ID")==0){
				printf("Opening file %s to retrieve particle ID information...\n",fld2);
				id=fopen(fld2,"r");
				if(id==0){
					printf("Failed to open file!\n");
					return(1);
				}
				blklen=0;
				for(i=0;i<6;i++) blklen += header_block.npart[i];
				blklen *= sizeof(unsigned int);
				process_uint_array(id,out,blklen,1);
				fclose(id);
			}else if(strcmp(fld1,"Mass")==0){
				printf("Opening file %s to retrieve mass information...\n",fld2);
				mass=fopen(fld2,"r");
				if(mass==0){
					printf("Failed to open file!\n");
					return(1);
				}
				//Calculate the block size, we only process those for which we don't already have a mass...
				blklen=0;
				for(i=0;i<6;i++)
				{
					if(header_block.mass[i]==0 && header_block.npart[i]>0)
						blklen+=header_block.npart[i];
				}
				blklen *= sizeof(float);
				process_float_array(mass,out,blklen,1);
				fclose(mass);
			}else if(strcmp(fld1,"InternalEnergy")==0){
				printf("Opening file %s to retrieve internal energy information...\n",fld2);
				energy=fopen(fld2,"r");
				if(energy==0){
					printf("Failed to open file!\n");
					return(1);
				}
				//Calculate the block size, we only need to do this for type 0 particles...
				blklen=header_block.npart[0]*sizeof(float);
				//We only need to do this if there are some gas particles present...
				if(blklen>0)
					process_float_array(energy,out,blklen,1);
				fclose(energy);
			}else if(strcmp(fld1,"Density")==0){
				printf("Opening file %s to retrieve density information...\n",fld2);
				density=fopen(fld2,"r");
				if(density==0){
					printf("Failed to open file!\n");
					return(1);
				}
				//Calculate the block size, we only need to do this for type 0 particles, i.e. gas particles...
				blklen=header_block.npart[0]*sizeof(float);
				//We only need to do this if there are some gas particles present...
				if(blklen>0)
					process_float_array(density,out,blklen,1);
				fclose(density);
			}else if(strcmp(fld1,"Smoothing")==0){
				printf("Opening file %s to retrieve SPH smoothing information...\n",fld2);
				smoothing=fopen(fld2,"r");
				if(smoothing==0){
					printf("Failed to open file!\n");
					return(1);
				}
				//Calculate the block size, we only need to do this for type 0 particles, i.e. gas particles...
				blklen=header_block.npart[0]*sizeof(float);
				//We only need to do this if there are some gas particles present...
				if(blklen>0)
					process_float_array(smoothing,out,blklen,1);
				fclose(smoothing);
			}else if(strcmp(fld1,"GravPot")==0){
				printf("Opening file %s to retrieve gravitational potential information...\n",fld2);
				pot=fopen(fld2,"r");
				if(pot==0){
					printf("Failed to open file!\n");
					return(1);
				}
				//Calculate the block size...
				blklen=0;
				for(i=0;i<6;i++) blklen += header_block.npart[i];
				blklen *= sizeof(float);
				process_float_array(pot,out,blklen,1);
				fclose(pot);
			}else if(strcmp(fld1,"Accel")==0){
				printf("Opening file %s to retrieve acceleration information...\n",fld2);
				accel=fopen(fld2,"r");
				if(accel==0){
					printf("Failed to open file!\n");
					return(1);
				}
				//Calculate the block size...
				blklen=0;
				for(i=0;i<6;i++) blklen += header_block.npart[i];
				blklen *= 3*sizeof(float);
				process_float_array(accel,out,blklen,3);
				fclose(accel);
			}else if(strcmp(fld1,"EntropyChange")==0){
				printf("Opening file %s to retrieve entropy change information...\n",fld2);
				entropy=fopen(fld2,"r");
				if(entropy==0){
					printf("Failed to open file!\n");
					return(1);
				}
				//Calculate the block size, we only need to do this for type 0 particles, i.e. gas particles...
				blklen=header_block.npart[0]*sizeof(float);
				//We only need to do this if there are some gas particles present...
				if(blklen>0)
					process_float_array(entropy,out,blklen,1);
				fclose(entropy);
			}else if(strcmp(fld1,"Timestep")==0){
				printf("Opening file %s to retrieve time step information...\n",fld2);
				time=fopen(fld2,"r");
				if(time==0){
					printf("Failed to open file!\n");
					return(1);
				}
				//Calculate the block size, we only need to do this for type 0 particles, i.e. gas particles...
				blklen=header_block.npart[0]*sizeof(float);
				//We only need to do this if there are some gas particles present...
				if(blklen>0)
					process_float_array(time,out,blklen,1);
				fclose(time);
			}else{
				printf("Unknown block name \"%s\", skipping...\n",fld1);
			}
		}
	}
	fclose(out);
	return(0);
}

/*This processes most of the other block types, taking input from appropriate text file*/

void process_float_array(FILE *input,FILE *output,int blklen,int Ndim)
{
	float dummy[3];
	char record[MAX_LEN_CHAR];
	/* The block length should already have been calculated and passed to this function*/
	BLKLEN;
	while(fgets(record, sizeof(record),input))
	{

		/*Skip the line if it's commented*/
		if(record[0]!='#'){
			if(Ndim==3)
			{
				sscanf(record,"%f\t%f\t%f\n",&dummy[0],&dummy[1],&dummy[2]);
				/*Each line gets written out as a block*/
				//dummy[0]=atof(strtok(record,"\t"));
				//dummy[1]=atof(strtok(NULL,"\t"));
				//dummy[2]=atof(strtok(NULL,"\n"));
				my_fwrite(dummy,sizeof(float),3,output);
			}else if(Ndim==2){
				sscanf(record,"%f\t%f\n",&dummy[0],&dummy[1]);
				my_fwrite(dummy,sizeof(float),2,output);
			}else if(Ndim==1){
				sscanf(record,"%f\n",&dummy[0]);
				my_fwrite(dummy,sizeof(float),1,output);
			}else{
				printf("An error has occured, Ndim must be 1,2 or 3\n");
				fflush(stdout);
			//	endrun(777);
			}
		}
	}
	/*Written them all...*/
	BLKLEN;
}

/*There is one block type which takes an unsigned int array...*/

void process_uint_array(FILE *input,FILE *output,int blklen,int Ndim)
{
	unsigned int dummy[3];
	char record[MAX_LEN_CHAR];
	/* The block length should already have been calculated and passed to this function*/
	BLKLEN;
	while(fgets(record, sizeof(record),input))
	{
		/*Skip the line if it's commented*/
		if(record[0]!='#'){
			if(Ndim==3)
			{
				sscanf(record,"%u\t%u\t%u\n",&dummy[0],&dummy[1],&dummy[2]);
				/*Each line gets written out as a block*/
				//dummy[0]=atoi(strtok(record,"\t"));
				//dummy[1]=atoi(strtok(NULL,"\t"));
				//dummy[2]=atoi(strtok(NULL,"\n"));
				my_fwrite(dummy,sizeof(unsigned int),3,output);
			}else if(Ndim==2){
				sscanf(record,"%u\t%u\n",&dummy[0],&dummy[1]);
				my_fwrite(dummy,sizeof(unsigned int),2,output);
			}else if(Ndim==1){
				sscanf(record,"%u\n",&dummy[0]);
				my_fwrite(dummy,sizeof(unsigned int),1,output);
			}else{
				printf("An error has occured, Ndim must be 1,2 or 3\n");
				fflush(stdout);
				//endrun(777);
			}
		}
	}
	/*Written them all...*/
	BLKLEN;
}

/*This processes the Header block from the appropriate text file*/

struct io_header process_header(FILE *input,FILE *output)
{
	char record[MAX_LEN_CHAR];
	int counter=0,blklen;
	struct io_header header;

	while(fgets(record, sizeof(record),input))
	{
		
		/*Make sure it's not a commented line*/
		if(record[0]!='#')
		{
			//printf("Processing case %i, with %s",counter,record);
			switch(counter)
			{
				/*Number of particles...*/
				case 0 : sscanf(record,"%u\t%u\t%u\t%u\t%u\t%u\n",&header.npart[0],&header.npart[1],&header.npart[2],&header.npart[3],&header.npart[4],&header.npart[5]);
							break;
				/*The masses...*/
				case 1 : sscanf(record,"%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n",&header.mass[0],&header.mass[1],&header.mass[2],&header.mass[3],&header.mass[4],&header.mass[5]);
							break;
				/*The time*/
				case 2:	sscanf(record,"%lf\n",&header.time);
							break;
				/*The redshift*/
				case 3:	sscanf(record,"%lf\n",&header.redshift);
							break;
				/*FlagSFR*/
				case 4:	sscanf(record,"%i\n",&header.flag_sfr);
							break;
				/*Flag Feedback*/
				case 5:	sscanf(record,"%i\n",&header.flag_feedback);
							break;
				/*Total number of particles*/
				case 6:	sscanf(record,"%u\t%u\t%u\t%u\t%u\t%u\n",&header.npartTotal[0],&header.npartTotal[1],&header.npartTotal[2],&header.npartTotal[3],&header.npartTotal[4],&header.npartTotal[5]);
							break;
				/*Flag Cooling*/
				case 7:	sscanf(record,"%i\n",&header.flag_cooling);
							break;
				/*Number of files*/
				case 8:	sscanf(record,"%i\n",&header.num_files);
							break;
				/*Boxsize*/
				case 9:	sscanf(record,"%lf\n",&header.BoxSize);
							break;
				/*Omega0*/
				case 10:	sscanf(record,"%lf\n",&header.Omega0);
							break;
				/*OmegaLambda*/
				case 11:	sscanf(record,"%lf\n",&header.OmegaLambda);
							break;
				/*Hubble Parameter*/
				case 12:	sscanf(record,"%lf\n",&header.HubbleParam);
							break;
				/*Flag Stellar Age*/
				case 13:	sscanf(record,"%i\n",&header.flag_stellarage);
							break;
				/*Flag Metals*/
				case 14:	sscanf(record,"%i\n",&header.flag_metals);
							break;
				/*High work total number of particles*/
				case 15:	sscanf(record,"%u\t%u\t%u\t%u\t%u\t%u\n",&header.nparttotalHighWord[0],&header.nparttotalHighWord[1],&header.nparttotalHighWord[2],&header.nparttotalHighWord[3],&header.nparttotalHighWord[4],&header.nparttotalHighWord[5]);
							break;
				/*Flag entropy instead of u*/
				case 16:	sscanf(record,"%i\n",&header.flag_entropy_instead_u);
							break;
				default:	printf("%s\n",record);
							break;
			}
			counter++;
		}
	}
	blklen=sizeof(header);
	BLKLEN;
	//This actually writes the bugger out to file...
	my_fwrite(&header, sizeof(header),1,output);
	BLKLEN;
	//All done...
	return(header);
}


/* This catches I/O errors occuring for my_my_fwrite(). In this case we better stop.
 */
size_t my_fwrite(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
  size_t nwritten;
  
  if((nwritten=fwrite(ptr, size, nmemb, stream))!=nmemb)
  {
    printf("I/O error (fwrite) on has occured.\n");
    fflush(stdout);
    /*endrun(777);*/
  }
  return nwritten;
}



