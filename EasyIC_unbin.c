#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define PI 3.141592654
#define BLKLEN my_fread(&blklen,sizeof(blklen),1,binaryFile);
#define PRINT_VNUM printf("This is EasyIC, v1.0.6\n");


size_t my_fread(void *ptr, size_t size, size_t nmemb, FILE *stream);
struct io_header process_header(FILE *input,FILE *output);
void write_header(struct io_header,char *);
void write_float_array_3d(float array[][3],int Dim1, char *filename);
void write_float_array_2d(float array[][2],int Dim1, char *filename);
void write_float_array_1d(float array[],int Dim1, char *filename);
void write_uint_array_1d(unsigned int array[],int Dim, char *filename);

/*This is the header as described in the Gadget2 documentation.  It should be 256 bytes in total length.  Check that the definitions match the latest version*/
struct io_header
{
  unsigned int npart[6];                        /*!< number of particles of each type in this file */
  double mass[6];                      /*!< mass of particles of each type. If 0, then the masses are explicitly
                                        stored in the mass-block of the snapshot file, otherwise they are omitted */
  double time;                         /*!< time of snapshot file */
  double redshift;                     /*!< redshift of snapshot file */
  int flag_sfr;                        /*!< flags whether the simulation was including star formation */
  int flag_feedback;                   /*!< flags whether feedback was included (obsolete) */
  unsigned int npartTotal[6];          /*!< total number of particles of each type in this snapshot. This can be
                                        different from npart if one is dealing with a multi-file snapshot. */
  int flag_cooling;                    /*!< flags whether cooling was included  */
  int num_files;                       /*!< number of files in multi-file snapshot */
  double BoxSize;                      /*!< box-size of simulation in case periodic boundaries were used */
  double Omega0;                       /*!< matter density in units of critical density */
  double OmegaLambda;                  /*!< cosmological constant parameter */
  double HubbleParam;                  /*!< Hubble parameter in units of 100 km/sec/Mpc */
  int flag_stellarage;                 /*!< flags whether the file contains formation times of star particles */
  int flag_metals;                     /*!< flags whether the file contains metallicity values for gas and star particles */
  unsigned int nparttotalHighWord[6];  /*!< High word of the total number of particles of each type */
  int  flag_entropy_instead_u;         /*!< flags that IC-file contains entropy instead of u */
  char fill[60];	               /*!< fills to 256 Bytes */
};

int main(int argc, char *argv[] )
{
	FILE *binaryFile, *file;
	struct io_header header_block;
	int blklen,Npart,Ngas,Nm,i,fileLen;
	char filename[2048],flag[4];

	if (argc <3 ) /* We should have two arguments, the program and the file */
	{
		if(argc==2 && (!strcmp(argv[1],"--version") || !strcmp(argv[1],"-v"))){
			PRINT_VNUM;
			return(1);
		}
		if(argc==2 && (!strcmp(argv[1],"--help") || !strcmp(argv[1],"-h"))){
			printf("\t%s takes a Gadget-2.0 binary file and writes text files containing the information contained within.  See the README and EXAMPLE for more information.\n",argv[0]);
			printf("\tThe usage is %s InputFile OutputDirectory [gart]\n",argv[0]);
			printf("\tPossible options are:\n");
			printf("\tInputFile \tA Gadget-2.0 formatted binary.\n");
			printf("\tOutputDirectory\tA directory (which must alread exist) where the text version of the Gadget-2.0 binary will be written.\n");
			printf("\tg\t\tOptional flag which indicates that the binary block contains [g]ravitational potential information.  This will only be the case if it is explicitly given by the makefile option.\n");
			printf("\ta\t\tOptional flag which indicates that the binary block contains [a]cceleration information.  This will only be the case if it is explicitly given by the makefile option.\n");
			printf("\tr\t\tOptional flag which indicates that the binary block contains ent[r]opy information.  This will only be the case if it is explicitly given by the makefile option.\n");
			printf("\tt\t\tOptional flag which indicates that the binary block contains the size of each [t]imestep.  This will only be the case if it is explicitly given by the makefile option.\n");
			printf("\t--version (-v)\t Prints version number.\n");
			printf("\t--help (-h)\t This help dialog.\n");
			return(1);
		}
		printf("usage: %s InputFilename OutputDirectory [gart]\n\t%s --help for more information.\n",argv[0],argv[0]);
		return(1);
	}
	binaryFile=fopen(argv[1],"r");
	//Get and store the file length
	fseek(binaryFile,0L,SEEK_END);
	fileLen=ftell(binaryFile);
	fseek(binaryFile,0L,SEEK_SET);
	//Load the optional arguments if they exist...
	strcpy(flag,"    ");
	if(argc==4)
		strcpy(flag,argv[3]);
	//We want to write a Filenames.txt file that will link everything togethor.  Open that for writing...
	strcpy(filename,argv[2]);
	strcat(filename,"//Filenames.txt");
	file=fopen(filename,"w");
	//A header...
	fprintf(file,"#This file should list all the blocks that you need for your initial conditions.  The format is BLOCKNAME<TAB>FILENAME.  Commented lines are ignored (obviously).\n");
	/*Loop through blocks...*/
	//Process the header...
	//Read the Fortran blocksize declaration
	printf("Processing Header block...\n");
	BLKLEN;
	//Load the header from the binary file..
	my_fread(&header_block,sizeof(header_block),1,binaryFile);
	//Finishing block
	BLKLEN;
	//Write it out to a text file
	strcpy(filename,argv[2]);
	strcat(filename,"//Header.txt");
	write_header(header_block,filename);
	//Add an entry to the filenames.txt
	fprintf(file,"Header\t%s\n",filename);
	//Process the position block...
	Npart=0;
	for(i=0;i<6;i++) Npart+=header_block.npart[i];
	//We limit the scope to avoid memory issues...
	{float pos[Npart][3];
	printf("Processing Position block...\n");
	BLKLEN;
	my_fread(pos,sizeof(float),Npart*3,binaryFile);
	BLKLEN;
	strcpy(filename,argv[2]);
	strcat(filename,"//Position.txt");
	write_float_array_3d(pos,Npart,filename);
	//Add an entry to the filenames.txt
	fprintf(file,"Pos\t%s\n",filename);}
	//Process the velocity block
	{float vel[Npart][3];
	printf("Processing Velocity block...\n");
	BLKLEN;
	my_fread(vel,sizeof(float),Npart*3,binaryFile);
	BLKLEN;
	strcpy(filename,argv[2]);
	strcat(filename,"//Velocity.txt");
	write_float_array_3d(vel,Npart,filename);
	//Add an entry to the filenames.txt
	fprintf(file,"Vel\t%s\n",filename);}
	//Process the particle ID block
	{unsigned int id[Npart];
	printf("Processing ID block...\n");
	BLKLEN;
	my_fread(id,sizeof(unsigned int),Npart,binaryFile);
	BLKLEN;
	strcpy(filename,argv[2]);
	strcat(filename,"//ID.txt");
	write_uint_array_1d(id,Npart,filename);
	//Add an entry to the filenames.txt
	fprintf(file,"ID\t%s\n",filename);}
	//Process the mass block, if it exists
	Nm=0;
	for(i=0;i<6;i++)
	{
		if(header_block.mass[i]==0 && header_block.npart[i]>0)
			Nm+=header_block.npart[i];
	}
	if(Nm)
	{
		printf("Processing Mass block...\n");
		float masses[Nm];
		BLKLEN;
		my_fread(masses,sizeof(float),Nm,binaryFile);
		BLKLEN;
		strcpy(filename,argv[2]);
		strcat(filename,"//Masses.txt");
		write_float_array_1d(masses,Nm,filename);
		//Add an entry to the filenames.txt
		fprintf(file,"Mass\t%s\n",filename);
	}
	//Process the internal energy block, if it exists
	Ngas=header_block.npart[0];
	if(Ngas)
	{
		printf("Processing Internal Energy block...\n");
		float u[Ngas];
		BLKLEN;
		my_fread(u,sizeof(float),Ngas,binaryFile);
		BLKLEN;
		strcpy(filename,argv[2]);
		strcat(filename,"//Energy.txt");
		write_float_array_1d(u,Ngas,filename);
		//Add an entry to the filenames.txt
		fprintf(file,"InternalEnergy\t%s\n",filename);
	}
	//From here on out we need to check to see if we've reached the end of file and stop if we have...
	//Process the density block, if it exists
	if(Ngas && ftell(binaryFile)!=fileLen)
	{
		printf("Processing Density block...\n");
		float rho[Ngas];
		BLKLEN;
		my_fread(rho,sizeof(float),Ngas,binaryFile);
		BLKLEN;
		strcpy(filename,argv[2]);
		strcat(filename,"//Density.txt");
		write_float_array_1d(rho,Ngas,filename);
		//Add an entry to the filenames.txt
		fprintf(file,"Density\t%s\n",filename);
	}
	//Smoothing length, if it exists
	if(Ngas && ftell(binaryFile)!=fileLen)
	{
		printf("Processing SPH Smoothing block...\n");
		float hsml[Ngas];
		BLKLEN;
		my_fread(hsml,sizeof(float),Ngas,binaryFile);
		BLKLEN;
		strcpy(filename,argv[2]);
		strcat(filename,"//Smoothing.txt");
		write_float_array_1d(hsml,Ngas,filename);
		//Add an entry to the filenames.txt
		fprintf(file,"Smoothing\t%s\n",filename);
	}
	//For the next four blocks, the makefile tells us if they exist or not.  Instead we rely on the user telling us via the optional third command line argument.  Stuff will break if the user lies obviously...
	//Gravitational potential, if it exists.
	//i=ftell(binaryFile);
	//printf("The file has length %i and we are at position %i\n",fileLen,i);
	if(strchr(flag,'g')!=NULL)
	{
		printf("Processing Gravitational Potential block...\n");
		float pot[Npart];
		BLKLEN;
		my_fread(pot,sizeof(float),Npart,binaryFile);
		BLKLEN;
		strcpy(filename,argv[2]);
		strcat(filename,"//GravPotential.txt");
		write_float_array_1d(pot,Npart,filename);
		//Add an entry to the filenames.txt
		fprintf(file,"GravPot\t%s\n",filename);
	}
	//Accelerations, if they exist
	if(strchr(flag,'a')!=NULL)
	{
		printf("Processing Accelaration block...\n");
		float acc[Npart][3];
		BLKLEN;
		my_fread(acc,sizeof(float),Npart*3,binaryFile);
		BLKLEN;
		strcpy(filename,argv[2]);
		strcat(filename,"//Acceleration.txt");
		write_float_array_3d(acc,Npart,filename);
		//Add an entry to the filenames.txt
		fprintf(file,"Accel\t%s\n",filename);
	}
	//Rate of entropy production, if it exists
	if(strchr(flag,'r')!=NULL && Ngas)
	{
		printf("Processing Entropy Production block...\n");
		float dAdt[Ngas];
		BLKLEN;
		my_fread(dAdt,sizeof(float),Ngas,binaryFile);
		BLKLEN;
		strcpy(filename,argv[2]);
		strcat(filename,"//EntropyChange.txt");
		write_float_array_1d(dAdt,Npart,filename);
		//Add an entry to the filenames.txt
		fprintf(file,"EntropyChange\t%s\n",filename);
	}
	//Timesteps of particles, if it exists
	if(strchr(flag,'t')!=NULL)
	{
		printf("Processing Timestep block...\n");
		float dt[Npart];
		BLKLEN;
		my_fread(dt,sizeof(float),Npart,binaryFile);
		BLKLEN;
		strcpy(filename,argv[2]);
		strcat(filename,"//Timesteps.txt");
		write_float_array_1d(dt,Npart,filename);
		//Add an entry to the filenames.txt
		fprintf(file,"Timestep\t%s\n",filename);
	}
	fprintf(file,"#END");
	fclose(file);
	return(0);
}

/* This will write out the header to a file given by the string filename*/
void write_header(struct io_header header_block, char *filename)
{
	FILE *file;
	int i;
	file=fopen(filename,"w");
	fprintf(file,"#The header block.  Defines everything else.\n");
	fprintf(file,"#The number of partciles in this file, listed from type 0 to 5\n");
	for(i=0;i<6;i++) fprintf(file,"%u\t",header_block.npart[i]);fprintf(file,"\n");
	fprintf(file,"#Mass array.  Either gives the mass of each particle type or 0.0 if defined on a per/particle basis.\n");
	for(i=0;i<6;i++) fprintf(file,"%.16G\t",header_block.mass[i]);fprintf(file,"\n");
	fprintf(file,"#Time.  For initial conditions this field has no meaning\n%.16G\n",header_block.time);
	fprintf(file,"#Redshift\n%.16G\n",header_block.redshift);
	fprintf(file,"#Star formation rate flag\n%i\n",header_block.flag_sfr);
	fprintf(file,"#Feedback flag\n%i\n",header_block.flag_feedback);
	fprintf(file,"#The total number of particles across all files for this simulation, for the 6 types\n"); 
	for(i=0;i<6;i++) fprintf(file,"%u\t",header_block.npartTotal[i]);fprintf(file,"\n");
	fprintf(file,"#Cooling flag\n%i\n",header_block.flag_cooling);
	fprintf(file,"#Total number of files is this simulation\n%i\n",header_block.num_files);
	fprintf(file,"#The box size\n%.16G\n",header_block.BoxSize);
	fprintf(file,"#Omega0\n%.16G\n",header_block.Omega0);
	fprintf(file,"#OmegaLambda\n%.16G\n",header_block.OmegaLambda);
	fprintf(file,"#Hubble Parameter\n%.16G\n",header_block.HubbleParam);
	fprintf(file,"#Star formation time flag\n%i\n",header_block.flag_stellarage);
	fprintf(file,"#Metals flag\n%i\n",header_block.flag_metals);
	fprintf(file,"#If the number of particles is 64 bits, this is the \" high word\" part of the total particle numbers\n");
	for(i=0;i<6;i++) fprintf(file,"%u\t",header_block.nparttotalHighWord[i]);fprintf(file,"\n");
	fprintf(file,"#Is entropy used intstead of internal energy?\n%i",header_block.flag_entropy_instead_u);
	fclose(file);
}

/* This will write out an array of floats, with 3 floats per entry, to filename*/
void write_float_array_3d(float array[][3],int Dim, char *filename)
{
	FILE *file;
	int i;
	file=fopen(filename,"w");
	for(i=0; i<Dim; i++)
	{
		fprintf(file,"%.8G\t%.8G\t%.8G\n",array[i][0],array[i][1],array[i][2]);
	}
	fprintf(file,"#END");
	fclose(file);
}

/* This will write out an array of floats, with 2 floats per entry, to filename*/
void write_float_array_2d(float array[][2],int Dim, char *filename)
{
	FILE *file;
	int i;
	file=fopen(filename,"w");
	for(i=0; i<Dim; i++)
	{
		fprintf(file,"%.8G\t%.8G\n",array[i][0],array[i][1]);
	}
	fprintf(file,"#END");
	fclose(file);
}

/* This will write out an array of floats, with 1 floats per entry, to filename*/
void write_float_array_1d(float array[],int Dim, char *filename)
{
	FILE *file;
	int i;
	file=fopen(filename,"w");
	for(i=0; i<Dim; i++)
	{
		fprintf(file,"%.8G\n",array[i]);
	}
	fprintf(file,"#END");
	fclose(file);
}

/* This will write out an array of unsigned ints, with 1 uints per entry, to filename*/
void write_uint_array_1d(unsigned int array[],int Dim, char *filename)
{
	FILE *file;
	int i;
	file=fopen(filename,"w");
	for(i=0; i<Dim; i++)
	{
		fprintf(file,"%u\n",array[i]);
	}
	fprintf(file,"#END");
	fclose(file);
}


/* This catches I/O errors occuring for fread(). In this case we better stop.
 */
size_t my_fread(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
  size_t nread;
  
  if((nread=fread(ptr, size, nmemb, stream))!=nmemb)
  {
    printf("I/O error (fread) has occured.\n");
    fflush(stdout);
    /*endrun(778);*/
  }
  return nread;
}
