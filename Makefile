CC=gcc
CFLAGS=-Wall -lm
SOURCES=EasyIC_bin.c EasyIC_unbin.c
INSTALL_LOCATION=~/Applications/bin

all:
	gcc -Wall -lm EasyIC_bin.c -o ICbin
	gcc -Wall -lm EasyIC_unbin.c -o ICunbin
clean:
	rm ICbin ICunbin
install:
	mv ICbin ${INSTALL_LOCATION}
	mv ICunbin ${INSTALL_LOCATION}
